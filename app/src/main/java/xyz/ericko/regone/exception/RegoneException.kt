package xyz.ericko.regone.exception

sealed class RegoneException(msg: String) : Exception(msg)
class ValidationException(validationError: String) : RegoneException(validationError)