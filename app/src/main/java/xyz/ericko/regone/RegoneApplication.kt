package xyz.ericko.regone

import android.app.Application
import net.danlew.android.joda.JodaTimeAndroid
import xyz.ericko.regone.db.RegoneDatabase

class RegoneApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        JodaTimeAndroid.init(this)
        RegoneDatabase.getInstance(this)
    }

}