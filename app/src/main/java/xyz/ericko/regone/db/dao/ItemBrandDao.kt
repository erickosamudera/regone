package xyz.ericko.regone.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Maybe
import xyz.ericko.regone.entity.ItemBrand

@Dao
interface ItemBrandDao {

    // write
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(brand: ItemBrand) : Long

    @Query("SELECT * FROM ItemBrand WHERE name == :name COLLATE NOCASE LIMIT 1")
    fun single(name: String) : Maybe<List<ItemBrand>>

    @Query("SELECT * FROM ItemBrand WHERE name LIKE :value || '%' COLLATE NOCASE ORDER BY name LIMIT 5")
    fun nameStartsWith(value: String) : Maybe<List<ItemBrand>>

    @Query("SELECT * FROM ItemBrand WHERE name LIKE '%' || :value || '%' COLLATE NOCASE ORDER BY name ASC LIMIT 5")
    fun nameContains(value: String) : Maybe<List<ItemBrand>>
}