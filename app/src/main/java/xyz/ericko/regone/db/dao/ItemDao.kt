package xyz.ericko.regone.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import io.reactivex.Maybe
import xyz.ericko.regone.entity.Item
import xyz.ericko.regone.entity.ItemQuickRead

@Dao
interface ItemDao {

    // read
    @Query("SELECT * FROM item WHERE uid == :uid")
    fun uid(uid: Long) : Maybe<List<ItemQuickRead>>

    // read
    @Query("SELECT * FROM item ORDER BY update_time DESC")
    fun all() : Maybe<List<ItemQuickRead>>
//
//    @Query("SELECT * FROM supplier WHERE name == :name COLLATE NOCASE LIMIT 1")
//    fun single(name: String) : Maybe<List<Supplier>>
//
//    @Query("SELECT * FROM supplier WHERE name LIKE :value || '%' COLLATE NOCASE ORDER BY name LIMIT 5")
//    fun nameStartsWith(value: String) : Maybe<List<Supplier>>
//
//    @Query("SELECT * FROM supplier WHERE name LIKE '%' || :value || '%' COLLATE NOCASE ORDER BY name ASC LIMIT 5")
//    fun nameContains(value: String) : Maybe<List<Supplier>>
//
//    // write
//
    @Insert
    fun insert(item: Item) : Long
//
//    @Update
//    fun update(supplier: Supplier) : Int
//
//    @Delete
//    fun delete(supplier: Supplier) : Int
}