package xyz.ericko.regone.db.dao

import android.arch.persistence.room.*
import io.reactivex.Maybe
import xyz.ericko.regone.entity.Supplier

@Dao
interface SupplierDao {

    // read
    @Query("SELECT * FROM supplier ORDER BY name ASC")
    fun all() : Maybe<List<Supplier>>

    @Query("SELECT * FROM supplier WHERE name == :name COLLATE NOCASE LIMIT 1")
    fun single(name: String) : Maybe<List<Supplier>>

    @Query("SELECT * FROM supplier WHERE name LIKE :value || '%' COLLATE NOCASE ORDER BY name LIMIT 5")
    fun nameStartsWith(value: String) : Maybe<List<Supplier>>

    @Query("SELECT * FROM supplier WHERE name LIKE '%' || :value || '%' COLLATE NOCASE ORDER BY name ASC LIMIT 5")
    fun nameContains(value: String) : Maybe<List<Supplier>>

    // write

    @Insert
    fun insert(supplier: Supplier) : Long

    @Update
    fun update(supplier: Supplier) : Int

    @Delete
    fun delete(supplier: Supplier) : Int
}