package xyz.ericko.regone.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context
import xyz.ericko.regone.db.dao.ItemBrandDao
import xyz.ericko.regone.db.dao.ItemDao
import xyz.ericko.regone.db.dao.ItemShapeDao
import xyz.ericko.regone.db.dao.SupplierDao
import xyz.ericko.regone.entity.*


@TypeConverters(RegoneTypeConverters::class)
@Database(entities = [
    Item::class,
    ItemBrand::class,
    ItemShape::class,
    ItemPrice::class,
    Supplier::class
], version = 1)
abstract class RegoneDatabase : RoomDatabase() {

    abstract fun supplierDao(): SupplierDao
    abstract fun itemShapeDao(): ItemShapeDao
    abstract fun itemBrandDao(): ItemBrandDao
    abstract fun itemDao(): ItemDao

    companion object {
        private var INSTANCE : RegoneDatabase? = null

        private fun initializeInstance(context: Context) {
            INSTANCE = Room.databaseBuilder(
                    context.applicationContext,
                    RegoneDatabase::class.java,
                    "regone-db"
            ).build()
        }

        fun getInstance(context: Context) : RegoneDatabase {
            if (INSTANCE == null) {
                initializeInstance(context)
            }
            return INSTANCE!!
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }

}