package xyz.ericko.regone.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Maybe
import xyz.ericko.regone.entity.ItemShape

@Dao
interface ItemShapeDao {

    // write
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(shape: ItemShape) : Long

    @Query("SELECT * FROM ItemShape WHERE name == :name COLLATE NOCASE LIMIT 1")
    fun single(name: String) : Maybe<List<ItemShape>>

    @Query("SELECT * FROM ItemShape WHERE name LIKE :value || '%' COLLATE NOCASE ORDER BY name LIMIT 5")
    fun nameStartsWith(value: String) : Maybe<List<ItemShape>>

    @Query("SELECT * FROM ItemShape WHERE name LIKE '%' || :value || '%' COLLATE NOCASE ORDER BY name ASC LIMIT 5")
    fun nameContains(value: String) : Maybe<List<ItemShape>>
}