package xyz.ericko.regone.db

import android.arch.persistence.room.TypeConverter
import org.joda.time.DateTime


class RegoneTypeConverters {

    @TypeConverter
    fun fromTimestamp (value: Long?) : DateTime? {
        if (value == null) {
            return null
        }
        return DateTime(value)
    }

    @TypeConverter
    fun toTimestamp(value: DateTime?) : Long? {
        if (value == null) {
            return null
        }
        return value.millis
    }

}