package xyz.ericko.regone.operation.validation

object Validator {

    fun validateName(name: String) : ValidationResult {
        if (name.isBlank()) {
            return ValidationResult(false, "Name can't be blank")
        }
        if (name.length > 50) {
            return ValidationResult(false, "Name too long")
        }
        return ValidationResult(true, "")
    }

}