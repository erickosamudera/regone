package xyz.ericko.regone.operation.item

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import xyz.ericko.regone.db.RegoneDatabase
import xyz.ericko.regone.entity.ItemShape

object ItemShapeOperations {

    fun getExistingIdOrInsertNew (
            regoneDatabase: RegoneDatabase,
            shapeName: String
    ) : Single<Long> {
        return regoneDatabase
                .itemShapeDao()
                .single(shapeName)
                .subscribeOn(Schedulers.io())
                .map {
                    if (it.isEmpty()) {
                        val newForm = ItemShape.generateNew(shapeName)
                        regoneDatabase.itemShapeDao().insert(newForm)
                    } else {
                        it.first().uid!!
                    }
                }
                .toSingle()
    }

    fun searchShape(
            regoneDatabase: RegoneDatabase,
            searchWord: String
    ) : Observable<List<ItemShape>> {
        if (searchWord.isBlank()) {
            return Observable.just(listOf())
        }

        val startsWithMatches = regoneDatabase.itemShapeDao().nameStartsWith(searchWord)
        val containsMatches = regoneDatabase.itemShapeDao().nameContains(searchWord)
        return startsWithMatches
                .mergeWith(containsMatches)
                .toObservable()
                .subscribeOn(Schedulers.io())
    }

}