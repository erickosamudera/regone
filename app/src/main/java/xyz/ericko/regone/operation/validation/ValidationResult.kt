package xyz.ericko.regone.operation.validation

data class ValidationResult(
        val result: Boolean,
        val error: String
)