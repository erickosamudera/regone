package xyz.ericko.regone.operation.supplier

import android.content.Context
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import xyz.ericko.regone.db.RegoneDatabase
import xyz.ericko.regone.entity.Supplier
import xyz.ericko.regone.entity.SupplierInputForm

object SupplierOperations {

    private const val LOG_TAG = "SupplierFormFragment"
    fun validateExistence (
            context: Context,
            supplierName: String
    ) : Single<Boolean> {
        val database = RegoneDatabase.getInstance(context)
        return database.supplierDao()
                .single(supplierName)
                .subscribeOn(Schedulers.io())
                .flatMapSingle {
                    val exist = ! it.isEmpty()
                    Single.just(exist)
                }
    }

    fun insertNewSupplier(
            context: Context,
            supplierForm: SupplierInputForm
    ) : Single<Long> {
        val database = RegoneDatabase.getInstance(context)
        return Single.fromCallable {
            database.supplierDao().insert( Supplier.generateNew(supplierForm.name) )
        }.subscribeOn(Schedulers.io())
    }

    fun updateSupplier(
            context: Context,
            existingSupplier: Supplier,
            supplierForm: SupplierInputForm
    ) : Single<Int> {
        val database = RegoneDatabase.getInstance(context)
        val updatedSupplier = existingSupplier.generateUpdate( supplierForm.name )
        return Single.fromCallable {
            database.supplierDao().update( updatedSupplier )
        }.subscribeOn(Schedulers.io())
    }

    fun deleteSupplier (
            context: Context,
            existingSupplier: Supplier
    ) : Single<Int> {
        val database = RegoneDatabase.getInstance(context)
        return Single.fromCallable {
            database.supplierDao().delete( existingSupplier )
        }.subscribeOn(Schedulers.io())
    }

    fun listAllSuppliers(
            context: Context
    ) : Maybe<List<Supplier>> {
        val database = RegoneDatabase.getInstance(context)
        return database.supplierDao()
                .all()
                .subscribeOn(Schedulers.io())
    }

    fun searchSupplier(
            context: Context,
            supplierName: String
    ) : Observable<List<Supplier>> {
        val database = RegoneDatabase.getInstance(context)

        if (supplierName.isBlank()) {
            return Observable.just(listOf())
        }

        val startsWithMatches = database.supplierDao().nameStartsWith(supplierName)
        val containsMatches = database.supplierDao().nameContains(supplierName)
        return startsWithMatches.mergeWith(containsMatches).toObservable().subscribeOn(Schedulers.io())
    }
}