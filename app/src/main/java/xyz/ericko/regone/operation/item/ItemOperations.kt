package xyz.ericko.regone.operation.item

import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import xyz.ericko.regone.db.RegoneDatabase
import xyz.ericko.regone.entity.Item
import xyz.ericko.regone.entity.ItemQuickRead

object ItemOperations {

    fun listAll(
            database: RegoneDatabase
    ) : Maybe<List<ItemQuickRead>> {
        return database.itemDao().all().subscribeOn(Schedulers.io())
    }

    fun insertItem(
            database: RegoneDatabase,
            shapeName: String,
            brandName: String,
            serial: String
    ) : Single<Long> {
        val brandIdSingle = ItemBrandOperations.getExistingIdOrInsertNew(database, brandName)
        val formIdSingle = ItemShapeOperations.getExistingIdOrInsertNew(database, shapeName)

        return brandIdSingle
                .zipWith<Long, Item>(
                    formIdSingle,
                    BiFunction { brandId, formId ->
                        Item.generateNew(formId, brandId, serial)
                    }
                )
                .subscribeOn(Schedulers.io())
                .map { newItem ->
                    database.itemDao().insert(newItem)
                }
    }

}