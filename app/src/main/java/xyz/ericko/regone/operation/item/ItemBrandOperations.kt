package xyz.ericko.regone.operation.item

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import xyz.ericko.regone.db.RegoneDatabase
import xyz.ericko.regone.entity.ItemBrand

object ItemBrandOperations {

    fun getExistingIdOrInsertNew (
            regoneDatabase: RegoneDatabase,
            brandName: String
    ) : Single<Long> {
        return regoneDatabase
                .itemBrandDao()
                .single(brandName)
                .subscribeOn(Schedulers.io())
                .map {
                    if (it.isEmpty()) {
                        val newBrand = ItemBrand.generateNew(brandName)
                        regoneDatabase.itemBrandDao().insert(newBrand)
                    } else {
                        it.first().uid!!
                    }
                }
                .toSingle()
    }

    fun searchBrand(
            regoneDatabase: RegoneDatabase,
            searchWord: String
    ) : Observable<List<ItemBrand>> {
        if (searchWord.isBlank()) {
            return Observable.just(listOf())
        }

        val startsWithMatches = regoneDatabase.itemBrandDao().nameStartsWith(searchWord)
        val containsMatches = regoneDatabase.itemBrandDao().nameContains(searchWord)
        return startsWithMatches
                .mergeWith(containsMatches)
                .toObservable()
                .subscribeOn(Schedulers.io())
    }

}