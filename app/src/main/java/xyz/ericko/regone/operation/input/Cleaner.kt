package xyz.ericko.regone.operation.input

object Cleaner {
    fun normaliseSpace(string: String) : String {
        return string.trim().replace(Regex("\\s{2,}"), " ")
    }
}