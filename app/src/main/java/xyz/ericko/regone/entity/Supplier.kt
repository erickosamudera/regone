package xyz.ericko.regone.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import org.joda.time.DateTime

@Entity
data class Supplier (
        val name : String,
        @ColumnInfo(name = "insert_time")
        val insertTime: DateTime,
        @ColumnInfo(name = "update_time")
        val updateTime: DateTime
) {
    @PrimaryKey (autoGenerate = true)
    var uid : Long? = null

    fun generateUpdate(newName: String) : Supplier {
        val currentUid = this.uid
        return Supplier(
                name = newName,
                insertTime = this.insertTime,
                updateTime = DateTime.now()
        ).apply {
            uid = currentUid
        }
    }

    companion object {
        fun generateEmpty() : Supplier {
            return generateNew("")
        }

        fun generateNew(name: String) : Supplier {
            return Supplier(name, DateTime.now(), DateTime.now())
        }
    }
}