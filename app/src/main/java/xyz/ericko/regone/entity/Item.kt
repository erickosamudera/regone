package xyz.ericko.regone.entity

import android.arch.persistence.room.*
import org.joda.time.DateTime

@Entity(
        foreignKeys = [
//            ForeignKey(
//                    entity = ItemShape::class,
//                    parentColumns = ["uid"],
//                    childColumns = ["shape_id"]
//            ),
//            ForeignKey(
//                    entity = ItemBrand::class,
//                    parentColumns = ["uid"],
//                    childColumns = ["brand_id"]
//            )
        ],
        indices = [
            Index("shape_id"),
            Index("brand_id"),
            Index("serial")
        ]
)
data class Item (
        @ColumnInfo(name = "shape_id")
        val shapeId : Long,
        @ColumnInfo(name = "brand_id")
        val brandId : Long,
        val serial : String,
        @ColumnInfo(name = "insert_time")
        val insertTime: DateTime,
        @ColumnInfo(name = "update_time")
        val updateTime: DateTime
) {
    @PrimaryKey (autoGenerate = true)
    var uid : Long? = null

    companion object {
        fun generateEmpty() : Item {
            return Item(-1, -1, "", DateTime.now(), DateTime.now())
        }
        fun generateNew(shapeId: Long, brandId: Long, serial: String) : Item {
            return Item(shapeId, brandId, serial, DateTime.now(), DateTime.now())
        }
    }
}

class ItemQuickRead {
    var uid: Long = 0
    lateinit var serial: String

    @ColumnInfo(name = "shape_id")
    var shapeId : Long = 0L
    @ColumnInfo(name = "brand_id")
    var brandId : Long = 0L

    @Relation(parentColumn = "brand_id", entityColumn = "uid")
    lateinit var brand: Set<ItemBrand>
    @Relation(parentColumn = "shape_id", entityColumn = "uid")
    lateinit var shape: Set<ItemShape>
    @ColumnInfo(name = "insert_time")
    lateinit var insertTime: DateTime
    @ColumnInfo(name = "update_time")
    lateinit var updateTime: DateTime

    fun getBrand(): ItemBrand {
        return brand.first()
    }
    fun getShape(): ItemShape {
        return shape.first()
    }
}