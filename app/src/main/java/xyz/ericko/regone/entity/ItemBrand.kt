package xyz.ericko.regone.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import org.joda.time.DateTime

@Entity
data class ItemBrand (
        val name : String,
        @ColumnInfo(name = "insert_time")
        val insertTime: DateTime,
        @ColumnInfo(name = "update_time")
        val updateTime: DateTime
) {
    @PrimaryKey (autoGenerate = true)
    var uid : Long? = null

    companion object {
        fun generateNew(name: String) : ItemBrand {
            return ItemBrand(name, DateTime.now(), DateTime.now())
        }
    }
}