package xyz.ericko.regone.entity

data class SupplierInputForm (
        val name: String
)
