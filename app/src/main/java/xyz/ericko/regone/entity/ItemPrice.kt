package xyz.ericko.regone.entity

import android.arch.persistence.room.*
import org.joda.time.DateTime

@Entity(
        foreignKeys = [
            ForeignKey(
                    entity = Item::class,
                    parentColumns = ["uid"],
                    childColumns = ["item_id"]
            ),
            ForeignKey(
                    entity = Supplier::class,
                    parentColumns = ["uid"],
                    childColumns = ["supplier_id"]
            )
        ],
        indices = [
            Index("item_id"),
            Index("supplier_id")
        ]
)
data class ItemPrice (
        @ColumnInfo(name = "item_id")
        val itemId : Long,
        @ColumnInfo(name = "supplier_id")
        val supplierId : Long,
        val serial : String,
        @ColumnInfo(name = "insert_time")
        val insertTime: DateTime,
        @ColumnInfo(name = "update_time")
        val updateTime: DateTime
) {
    @PrimaryKey (autoGenerate = true)
    var uid : Long? = null

    // POJO, populate manually later
    @Ignore
    var item: Item? = null

    @Ignore
    var supplier: Supplier? = null
}