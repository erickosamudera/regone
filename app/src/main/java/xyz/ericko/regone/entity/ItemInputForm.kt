package xyz.ericko.regone.entity

data class ItemInputForm (
        val brandName: String,
        val formName: String,
        val serial: String
)
