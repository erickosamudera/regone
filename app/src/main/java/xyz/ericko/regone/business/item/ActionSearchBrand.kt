package xyz.ericko.regone.business.item

import io.reactivex.Observable
import xyz.ericko.regone.business.abs.SearchAction
import xyz.ericko.regone.db.RegoneDatabase
import xyz.ericko.regone.entity.ItemBrand
import xyz.ericko.regone.operation.input.Cleaner.normaliseSpace
import xyz.ericko.regone.operation.item.ItemBrandOperations.searchBrand

class ActionSearchBrand (
        private val normaliseSpace: (String) -> String,
        private val searchBrand: (RegoneDatabase, String) -> Observable<List<ItemBrand>>,
        private val regoneDatabase: RegoneDatabase
) : SearchAction<ItemBrand> {
    override fun doIt(keyword: String) : Observable<List<ItemBrand>> {
        val cleanedKeyword = normaliseSpace(keyword)
        return searchBrand(regoneDatabase , cleanedKeyword)
    }

    companion object {
        fun quickCreate(
                regoneDatabase: RegoneDatabase
        ) : ActionSearchBrand {
            return ActionSearchBrand(
                    ::normaliseSpace,
                    ::searchBrand,
                    regoneDatabase
            )
        }
    }
}