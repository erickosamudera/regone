package xyz.ericko.regone.business.item

import io.reactivex.Observable
import xyz.ericko.regone.business.abs.SearchAction
import xyz.ericko.regone.db.RegoneDatabase
import xyz.ericko.regone.entity.ItemShape
import xyz.ericko.regone.operation.input.Cleaner.normaliseSpace
import xyz.ericko.regone.operation.item.ItemShapeOperations.searchShape

class ActionSearchForm (
        private val normaliseSpace: (String) -> String,
        private val searchShape: (RegoneDatabase, String) -> Observable<List<ItemShape>>,
        private val regoneDatabase: RegoneDatabase
) : SearchAction<ItemShape> {
    override fun doIt(keyword: String) : Observable<List<ItemShape>> {
        val cleanedKeyword = normaliseSpace(keyword)
        return searchShape(regoneDatabase, cleanedKeyword)
    }

    companion object {
        fun quickCreate(
                regoneDatabase: RegoneDatabase
        ) : ActionSearchForm {
            return ActionSearchForm(
                    ::normaliseSpace,
                    ::searchShape,
                    regoneDatabase
            )
        }
    }
}