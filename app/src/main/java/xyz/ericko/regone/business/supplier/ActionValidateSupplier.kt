package xyz.ericko.regone.business.supplier

import android.content.Context
import io.reactivex.Single
import xyz.ericko.regone.operation.input.Cleaner.normaliseSpace
import xyz.ericko.regone.operation.supplier.SupplierOperations.validateExistence
import xyz.ericko.regone.operation.validation.ValidationResult
import xyz.ericko.regone.operation.validation.Validator.validateName

class ActionValidateSupplier(
        private val normaliseSpace: (String) -> String,
        private val validateNameFormat: (String) -> ValidationResult,
        private val validateExistence: (Context, String) -> Single<Boolean>,
        private val context: Context,
        private val inputName: String
) {
    fun doIt() : Single<ValidationResult> {
        val supplierName = normaliseSpace(inputName)
        val validationResult = validateNameFormat(supplierName)

        if (! validationResult.result) {
            return Single.just(validationResult)
        }

        return validateExistence(
                context,
                supplierName
        ).map { existence ->
            val message = if (existence) { "Supplier already exists" } else { "" }
            val valid = ! existence
            ValidationResult(valid, message)
        }
    }

    companion object {
        fun quickCreate(
                inputName: String,
                context: Context
        ) : ActionValidateSupplier {
            return ActionValidateSupplier(
                    ::normaliseSpace,
                    ::validateName,
                    ::validateExistence,
                    context,
                    inputName
            )
        }
    }
}