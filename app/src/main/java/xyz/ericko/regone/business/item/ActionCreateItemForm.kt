package xyz.ericko.regone.business.item

import xyz.ericko.regone.entity.ItemInputForm
import xyz.ericko.regone.operation.input.Cleaner.normaliseSpace

class ActionCreateItemForm(
        private val normaliseSpace: (String) -> (String),
        private val brandName: String,
        private val shapeName: String,
        private val serial: String
) {
    fun doIt() : ItemInputForm {
        val finalBrandName = normaliseSpace(brandName)
        val finalShapeName = normaliseSpace(shapeName)
        val finalSerial = normaliseSpace(serial)
        return ItemInputForm(finalBrandName, finalShapeName, finalSerial)
    }

    companion object {
        fun quickCreate(
                brandName: String,
                shapeName: String,
                serial: String
        ) : ActionCreateItemForm {
            return ActionCreateItemForm(
                    ::normaliseSpace,
                    brandName,
                    shapeName,
                    serial
            )
        }
    }
}