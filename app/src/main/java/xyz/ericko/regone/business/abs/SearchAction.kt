package xyz.ericko.regone.business.abs

import io.reactivex.Observable

interface SearchAction<T> {
    fun doIt(keyword: String) : Observable<List<T>>
}