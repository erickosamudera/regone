package xyz.ericko.regone.business.supplier

import xyz.ericko.regone.entity.SupplierInputForm
import xyz.ericko.regone.operation.input.Cleaner.normaliseSpace

class ActionCreateSupplierForm(
        private val normaliseSpace: (String) -> (String),
        private val inputName: String
) {
    fun doIt() : SupplierInputForm {
        val supplierName = normaliseSpace(inputName)
        return SupplierInputForm(supplierName)
    }

    companion object {
        fun quickCreate(
                inputName: String
        ) : ActionCreateSupplierForm {
            return ActionCreateSupplierForm(
                    ::normaliseSpace,
                    inputName
            )
        }
    }
}