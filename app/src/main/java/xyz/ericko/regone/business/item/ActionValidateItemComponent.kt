package xyz.ericko.regone.business.item

import io.reactivex.Single
import xyz.ericko.regone.operation.input.Cleaner.normaliseSpace
import xyz.ericko.regone.operation.validation.ValidationResult
import xyz.ericko.regone.operation.validation.Validator.validateName

class ActionValidateItemComponent(
        private val normaliseSpace: (String) -> String,
        private val validateNameFormat: (String) -> ValidationResult,
        private val inputName: String
) {
    fun doIt() : Single<ValidationResult> {
        val supplierName = normaliseSpace(inputName)
        val validationResult = validateNameFormat(supplierName)
        return Single.just(validationResult)
    }

    companion object {
        fun quickCreate(
                inputName: String
        ) : ActionValidateItemComponent {
            return ActionValidateItemComponent(
                    ::normaliseSpace,
                    ::validateName,
                    inputName
            )
        }
    }
}