package xyz.ericko.regone.business.supplier

import android.content.Context
import io.reactivex.Observable
import xyz.ericko.regone.business.abs.SearchAction
import xyz.ericko.regone.entity.Supplier
import xyz.ericko.regone.operation.input.Cleaner.normaliseSpace
import xyz.ericko.regone.operation.supplier.SupplierOperations.searchSupplier

class ActionSearchSupplier (
        private val normaliseSpace: (String) -> String,
        private val searchSupplier: (Context, String) -> Observable<List<Supplier>>,
        private val context: Context
) : SearchAction<Supplier> {
    override fun doIt(keyword: String) : Observable<List<Supplier>> {
        val cleanedKeyword = normaliseSpace(keyword)
        return searchSupplier(
                context,
                cleanedKeyword
        )
    }

    companion object {
        fun quickCreate(
                context: Context
        ) : ActionSearchSupplier {
            return ActionSearchSupplier(
                    ::normaliseSpace,
                    ::searchSupplier,
                    context
            )
        }
    }
}