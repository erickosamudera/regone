package xyz.ericko.regone.ui.fragment.page

import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageButton
import android.widget.SearchView
import android.widget.TextView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import xyz.ericko.regone.R
import xyz.ericko.regone.business.abs.SearchAction
import xyz.ericko.regone.entity.Supplier
import xyz.ericko.regone.ui.adapter.SupplierAdapterBuilder
import xyz.ericko.regone.ui.behavior.SupplierUIBehaviorBuilder
import xyz.ericko.regone.ui.fragment.component.ListFragment
import java.util.concurrent.TimeUnit

class SearchWidgetFragment : Fragment(), SearchView.OnQueryTextListener {

    lateinit var supportFragmentManager: FragmentManager
    lateinit var searchAction : SearchAction<Supplier>
    lateinit var uiBehaviorBuilder: SupplierUIBehaviorBuilder
    lateinit var onSwitchToSearch: () -> Unit

    private var searchObserver : BehaviorSubject<String> = BehaviorSubject.create()
    private var pageDisposables : MutableList<Disposable> = mutableListOf()
    private var activeMode: WidgetMode = WidgetMode.BUTTON
    private lateinit var fragmentView: View
    private lateinit var searchButton: ImageButton
    private lateinit var backButton: ImageButton
    private lateinit var titleText: TextView
    private lateinit var searchView: SearchView
    private lateinit var searchResultFragment : ListFragment
    private lateinit var adapterBuilder: SupplierAdapterBuilder

    private enum class WidgetMode {
        BUTTON,
        SEARCH
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        prepareObserver()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        fragmentView = inflater!!.inflate(R.layout.fragment_button_search, container, false)
        searchButton = fragmentView.findViewById(R.id.fragment_widget_search_icon)
        backButton = fragmentView.findViewById(R.id.fragment_widget_search_back)
        titleText = fragmentView.findViewById(R.id.fragment_widget_search_title)

        searchView = fragmentView.findViewById(R.id.fragment_widget_search_input)
        searchView.setOnQueryTextListener(this)

        fragmentView.setOnClickListener { switchToSearchMode() }
        backButton.setOnClickListener { switchToButtonMode() }

        return fragmentView
    }

    override fun onDestroyView() {
        super.onDestroyView()
        pageDisposables.forEach { it.dispose() }
    }

    private fun switchToSearchMode() {
        if (activeMode == WidgetMode.BUTTON) {
            onSwitchToSearch()

            adapterBuilder = uiBehaviorBuilder.build(
                disposableList = pageDisposables,
                onInsertSuccess = {},
                onDeleteSuccess = { redoSearch() },
                onUpdateSuccess = { redoSearch() },
                fragmentManager = fragmentManager,
                supportFragmentManager = supportFragmentManager
            ).adapterBuilder

            searchButton.visibility = GONE
            titleText.visibility = GONE

            backButton.visibility = VISIBLE
            searchView.setQuery("", false)
            searchView.visibility = VISIBLE
            requestFocusOnSearchView()

            openSearchResultFragment()

            activeMode = WidgetMode.SEARCH
        }
    }

    private fun switchToButtonMode() {
        if (activeMode == WidgetMode.SEARCH) {
            searchButton.visibility = VISIBLE
            titleText.visibility = VISIBLE

            backButton.visibility = GONE
            searchView.setQuery("", false)
            searchView.visibility = GONE
            dismissKeyboardIfActive()

            activeMode = WidgetMode.BUTTON
            fragmentManager.popBackStack()
        }
    }

    private fun openSearchResultFragment() {
        searchResultFragment = ListFragment().apply {
            onListDismiss = {
                switchToButtonMode()
            }
        }

        fragmentManager.beginTransaction()
                .add(R.id.main_fragment_container, searchResultFragment)
                .addToBackStack("Search Result")
                .commit()
    }

    private fun prepareObserver() {
        val observerDisposable = searchObserver.debounce(300, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
                .switchMap { searchAction.doIt(it) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    val adapter = adapterBuilder.build( it )
                    searchResultFragment.updateAdapterData( adapter )
                }
        pageDisposables.add(observerDisposable)
    }

    private fun redoSearch() {
        val keyword = searchView.query.toString()
        searchAction.doIt(keyword = keyword)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    val adapter = adapterBuilder.build( it )
                    searchResultFragment.updateAdapterData( adapter )
                }
    }

    /**
     * Yes, this is weird.
     */
    private fun requestFocusOnSearchView() {
        searchView.isIconified = false
    }

    private fun dismissKeyboardIfActive() {
        val inputMethodManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (inputMethodManager.isAcceptingText) {
            val windowToken = activity.currentFocus.windowToken
            inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
        }
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        searchObserver.onNext(query ?: "")
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        searchObserver.onNext(newText ?: "")
        return true
    }

}

