package xyz.ericko.regone.ui.fragment.component.dialog

import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import xyz.ericko.regone.R
import xyz.ericko.regone.ui.adapter.RecyclerAction
import xyz.ericko.regone.ui.adapter.SupplierActionAdapter

class MenuContextDialog : BottomSheetDialogFragment() {

    lateinit var title : String
    lateinit var actions : List<RecyclerAction>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val fragmentView = inflater.inflate(R.layout.fragment_bottom_context, container, false)
        val titleTextbox = fragmentView.findViewById<TextView>(R.id.fragment_bottom_title)
        titleTextbox.text = title

        val actionAdapter = SupplierActionAdapter(actions)
        val actionView = fragmentView.findViewById<RecyclerView>(R.id.fragment_bottom_action)
        actionView.layoutManager = LinearLayoutManager(context)
        actionView.swapAdapter(actionAdapter, true)

        return fragmentView
    }


}