package xyz.ericko.regone.ui.fragment.component.form

import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.EditText
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import xyz.ericko.regone.R
import xyz.ericko.regone.business.item.ActionCreateItemForm
import xyz.ericko.regone.business.item.ActionSearchBrand
import xyz.ericko.regone.business.item.ActionSearchForm
import xyz.ericko.regone.business.item.ActionValidateItemComponent
import xyz.ericko.regone.db.RegoneDatabase
import xyz.ericko.regone.entity.Item
import xyz.ericko.regone.entity.ItemInputForm
import xyz.ericko.regone.operation.input.Cleaner
import xyz.ericko.regone.ui.helper.InputObserverBuilder

class ItemFormFragment : DialogFragment() {

    lateinit var onConfirmListener: (ItemInputForm) -> Unit
    var existingItem: Item = Item.generateEmpty()

    private val fragmentDisposables : MutableList<Disposable> = mutableListOf()

    private lateinit var addItemLayout: View
    private lateinit var alertDialog: AlertDialog

    private var isBrandValid = false
    private var isFormValid = false
    private var isSerialValid = false
    private val validatorObserver: PublishSubject<Unit> = PublishSubject.create()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialogBuilder = AlertDialog.Builder(context)
        val regoneDatabase = RegoneDatabase.getInstance(context)

        val inflater = (context as AppCompatActivity).layoutInflater
        addItemLayout = inflater.inflate(R.layout.fragment_add_item, null)

        val brandEditText = addItemLayout.findViewById<AutoCompleteTextView>(R.id.fragment_add_item_brand)
        val brandObserver = createBrandInputObserver(regoneDatabase, brandEditText)
        brandEditText.addTextChangedListener(ItemTextWatcher(brandObserver))

        val shapeEditText = addItemLayout.findViewById<AutoCompleteTextView>(R.id.fragment_add_item_shape)
        val shapeObserver = createFormInputObserver(regoneDatabase, shapeEditText)
        shapeEditText.addTextChangedListener(ItemTextWatcher(shapeObserver))

        val serialEditText = addItemLayout.findViewById<EditText>(R.id.fragment_add_item_serial)
        val serialObserver = createSerialInputObserver(serialEditText)
        serialEditText.addTextChangedListener(ItemTextWatcher(serialObserver))

        alertDialog = dialogBuilder
                .setView(addItemLayout)
                .setNegativeButton( "Batal" , null )
                .setPositiveButton( getPositiveButtonText() ) { _, _ ->
                    val form = ActionCreateItemForm.quickCreate(
                            brandEditText.text.toString(),
                            shapeEditText.text.toString(),
                            serialEditText.text.toString()
                    ).doIt()
                    onConfirmListener(form)
                }.create()

        val validatorDisposable = validatorObserver.switchMap {
            Single.just(isBrandValid && isFormValid && isSerialValid).toObservable()
        }.subscribe {
            val positiveButton = alertDialog.getButton(Dialog.BUTTON_POSITIVE)
            positiveButton.isEnabled = it
        }
        fragmentDisposables.add(validatorDisposable)

        return alertDialog
    }

    override fun onStart() {
        super.onStart()
        val positiveButton = alertDialog.getButton(Dialog.BUTTON_POSITIVE)
        positiveButton.isEnabled = false
    }

    private fun getPositiveButtonText() : String {
        return if (existingItem.uid == null) {
            "Tambahkan"
        } else {
            "Ubah"
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        fragmentDisposables.forEach { it.dispose() }
    }

    companion object {
        private const val LOG_TAG = "ItemFormFragment"
    }

    private fun createBrandInputObserver(
            regoneDatabase: RegoneDatabase,
            brandEditText: AutoCompleteTextView
    ) : BehaviorSubject<String> {
        return InputObserverBuilder()
                .attachAutocomplete(
                        autocompleteProcess = { ActionSearchBrand.quickCreate(regoneDatabase).doIt(it) },
                        onAutocomplete = {
                            val brandList = it.map { it.name }
                            val adapter = ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, brandList)
                            brandEditText.setAdapter(adapter)
                        }
                )
                .attachValidation(
                        validationProcess = { ActionValidateItemComponent.quickCreate(it).doIt().toObservable() },
                        onValidationSuccess = {
                            isBrandValid = true
                            validatorObserver.onNext(Unit)
                        },
                        onValidationFail = {
                            brandEditText.error = it
                            isBrandValid = false
                            validatorObserver.onNext(Unit)
                        }
                )
                .build()
    }

    private fun createFormInputObserver(
            regoneDatabase: RegoneDatabase,
            formEditText: AutoCompleteTextView
    ) : BehaviorSubject<String> {
        return InputObserverBuilder()
                .attachAutocomplete(
                        autocompleteProcess = { ActionSearchForm.quickCreate(regoneDatabase).doIt(it) },
                        onAutocomplete = {
                            val formList = it.map { it.name }
                            val adapter = ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, formList)
                            formEditText.setAdapter(adapter)
                        }
                )
                .attachValidation(
                        validationProcess = { ActionValidateItemComponent.quickCreate(it).doIt().toObservable() },
                        onValidationSuccess = {
                            isFormValid = true
                            validatorObserver.onNext(Unit)
                        },
                        onValidationFail = {
                            formEditText.error = it
                            isFormValid = false
                            validatorObserver.onNext(Unit)
                        }
                )
                .build()
    }

    private fun createSerialInputObserver(
            serialEditText: EditText
    ) : BehaviorSubject<String> {
        return InputObserverBuilder()
                .attachValidation(
                        validationProcess = { ActionValidateItemComponent.quickCreate(it).doIt().toObservable() },
                        onValidationSuccess = {
                            isSerialValid = true
                            validatorObserver.onNext(Unit)
                        },
                        onValidationFail = {
                            serialEditText.error = it
                            isSerialValid = false
                            validatorObserver.onNext(Unit)
                        }
                )
                .build()
    }
}

class ItemTextWatcher(
        private val inputObserver: BehaviorSubject<String>
) : TextWatcher {

    override fun afterTextChanged(s: Editable?) {
        if (s == null) {
            return
        }

        val cleanedText = Cleaner.normaliseSpace(s.toString())
        inputObserver.onNext(cleanedText)
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

}
