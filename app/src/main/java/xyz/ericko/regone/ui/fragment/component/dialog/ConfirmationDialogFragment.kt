package xyz.ericko.regone.ui.fragment.component.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.content.DialogInterface
import android.os.Bundle
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit

class ConfirmationDialogFragment : DialogFragment() {

    lateinit var message: String
    lateinit var onConfirm: (DialogInterface, Int) -> Unit
    var confirmTimerSeconds : Int = 0
    private var currentSecond : Int = 0

    lateinit var alertDialog: AlertDialog

    val dialogDisposables: MutableList<Disposable> = mutableListOf()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        currentSecond = confirmTimerSeconds

        alertDialog = AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton( createConfirmMessage(confirmTimerSeconds), onConfirm )
                .setNegativeButton("Batal", null)
                .create()

        return alertDialog
    }

    override fun onStart() {
        super.onStart()

        val positiveButton = alertDialog.getButton(Dialog.BUTTON_POSITIVE)
        if (confirmTimerSeconds > 0) {
            positiveButton.isEnabled = false

            val timerDisposable = Observable.interval(1, TimeUnit.SECONDS)
                    .take(confirmTimerSeconds.toLong())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnComplete {
                        positiveButton.isEnabled = true
                    }
                    .subscribe {
                        currentSecond -= 1
                        positiveButton.text = createConfirmMessage(currentSecond)
                    }

            dialogDisposables.add(timerDisposable)
        }
    }

    private fun createConfirmMessage(seconds: Int) : String {
        val format = if (seconds != 0) { "Ya {%d}" } else "Ya"
        return String.format(format, seconds)
    }

    override fun onStop() {
        super.onStop()
        dialogDisposables.forEach {
            it.dispose()
        }
    }
}