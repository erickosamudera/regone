package xyz.ericko.regone.ui.fragment.component.dialog.builder

import android.app.DialogFragment
import io.reactivex.disposables.Disposable
import xyz.ericko.regone.operation.supplier.SupplierOperations
import xyz.ericko.regone.ui.fragment.component.form.SupplierFormFragment

class SupplierAddDialogBuilder(
        private val disposableList: MutableList<Disposable>,
        private val onInsertSuccess: () -> Unit
) {
    val fragmentTag = "Add Supplier Dialog"

    fun build() : DialogFragment {
        return SupplierFormFragment().apply {
            this.onConfirmListener = {
                val insertDisposable = SupplierOperations.insertNewSupplier(
                        context = context,
                        supplierForm = it
                ).subscribe { _, exception ->
                    if (exception == null) {
                        onInsertSuccess()
                    }
                }
                disposableList.add(insertDisposable)
            }
        }
    }
}