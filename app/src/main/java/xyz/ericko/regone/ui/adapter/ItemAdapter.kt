package xyz.ericko.regone.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import xyz.ericko.regone.R
import xyz.ericko.regone.entity.ItemQuickRead

class ItemAdapter (
        private val dataset: List<ItemQuickRead>,
        private val onItemClick : (ItemQuickRead) -> Unit = {}
) :
        RecyclerView.Adapter<ItemAdapter.ViewHolder>()
{


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class ViewHolder(val adapterView: View) : RecyclerView.ViewHolder(adapterView)


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ItemAdapter.ViewHolder {
        val adapterView = LayoutInflater.from(parent.context)
                .inflate(R.layout.adapter_dummy, parent, false)
        adapterView.setOnClickListener {
            val item = it.getTag(R.id.item_data) as ItemQuickRead
            onItemClick(item)
        }

        // set the view's size, margins, paddings and layout parameters
        return ViewHolder(adapterView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        val item = dataset[position]

        val entryView = holder.adapterView.findViewById<View>(R.id.adapter_dummy_entry)
        entryView.setTag(R.id.item_data, item)

        val textView = holder.adapterView.findViewById<TextView>(R.id.adapter_dummy_text)
        textView.text = String.format("%s %s %s", item.getBrand().name, item.getShape().name, item.serial)
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataset.size

    companion object {
        private val LOG_TAG = "ItemAdapter"
    }

}

class ItemAdapterBuilder(
        private val onItemClick: (ItemQuickRead) -> Unit
) {
    fun build(dataset: List<ItemQuickRead>) : RecyclerView.Adapter<RecyclerView.ViewHolder> {
        return ItemAdapter(
                dataset = dataset,
                onItemClick = onItemClick
        ) as RecyclerView.Adapter<RecyclerView.ViewHolder>
    }
}

