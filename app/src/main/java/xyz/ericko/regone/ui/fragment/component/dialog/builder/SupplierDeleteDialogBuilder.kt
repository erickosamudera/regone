package xyz.ericko.regone.ui.fragment.component.dialog.builder

import android.app.DialogFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import xyz.ericko.regone.entity.Supplier
import xyz.ericko.regone.operation.supplier.SupplierOperations
import xyz.ericko.regone.ui.fragment.component.dialog.ConfirmationDialogFragment

class SupplierDeleteDialogBuilder(
        private val disposableList: MutableList<Disposable>,
        private val onDeleteSuccess: () -> Unit
) {
    val fragmentTag = "Delete Supplier Dialog"

    fun build(
            existingSupplier: Supplier
    ) : DialogFragment {
        return ConfirmationDialogFragment().apply {
            message = "Ini bakal dihapus lho!"
            confirmTimerSeconds = 10
            onConfirm = { _, _ ->
                val deleteDisposable = SupplierOperations.deleteSupplier(
                                context = context,
                                existingSupplier = existingSupplier
                        ).observeOn(AndroidSchedulers.mainThread())
                        .subscribe { _, exception ->
                            if (exception == null) {
                                onDeleteSuccess()
                            }
                        }
                disposableList.add(deleteDisposable)
            }
        }
    }
}