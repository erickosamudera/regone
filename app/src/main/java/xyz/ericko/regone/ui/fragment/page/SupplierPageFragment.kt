package xyz.ericko.regone.ui.fragment.page

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import xyz.ericko.regone.R
import xyz.ericko.regone.operation.supplier.SupplierOperations
import xyz.ericko.regone.ui.behavior.SupplierUIBehavior
import xyz.ericko.regone.ui.behavior.SupplierUIBehaviorBuilder
import xyz.ericko.regone.ui.fragment.component.ListFragment

class SupplierPageFragment : PageFragment() {

    private lateinit var pageLayout: View
    private lateinit var supplierListFragment: ListFragment
    private var pageDisposables : MutableList<Disposable> = mutableListOf()

    private lateinit var supplierUIBehavior: SupplierUIBehavior

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supplierListFragment = ListFragment()

        childFragmentManager.beginTransaction()
                .replace( R.id.fragment_page_supplier_list, supplierListFragment )
                .commit()

        supplierUIBehavior = SupplierUIBehaviorBuilder().build(
                disposableList = pageDisposables,
                onInsertSuccess = { displayAllSuppliersOnList() },
                onUpdateSuccess = { displayAllSuppliersOnList() },
                onDeleteSuccess = { displayAllSuppliersOnList() },
                fragmentManager = fragmentManager,
                supportFragmentManager = this.supportFragmentManager
        )
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        pageLayout = inflater!!.inflate(R.layout.fragment_page_supplier, container, false)
        this.prepareAddSupplierButton()
        return pageLayout
    }

    override fun onStart() {
        super.onStart()
        displayAllSuppliersOnList()
    }

    private fun displayAllSuppliersOnList() {
        val listDisposable = SupplierOperations.listAllSuppliers( supplierListFragment.context )
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { supplierList ->
                    supplierListFragment.updateAdapterData( supplierUIBehavior.adapterBuilder.build(supplierList) )
                }
        pageDisposables.add(listDisposable)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        pageDisposables.forEach { it.dispose() }
    }

    private fun prepareAddSupplierButton() : FloatingActionButton {
        val addButton = pageLayout.findViewById<FloatingActionButton>(R.id.fragment_page_supplier_button_add)
        addButton.setOnClickListener {
            val addDialogBuilder = supplierUIBehavior.addDialogBuilder
            addDialogBuilder.build().show(childFragmentManager, addDialogBuilder.fragmentTag)
        }
        return addButton
    }

    companion object {
        private val LOG_TAG = "SupplierPageFragment"
    }
}

