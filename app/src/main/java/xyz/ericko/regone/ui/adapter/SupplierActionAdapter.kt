package xyz.ericko.regone.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import xyz.ericko.regone.R

class SupplierActionAdapter(
        private val actions: List<RecyclerAction>
) : RecyclerView.Adapter<SupplierActionAdapter.ViewHolder>() {


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class ViewHolder(val adapterView: View) : RecyclerView.ViewHolder(adapterView)

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): SupplierActionAdapter.ViewHolder {
        val adapterView = LayoutInflater.from(parent.context)
                .inflate(R.layout.adapter_action, parent, false)

        // set the view's size, margins, paddings and layout parameters
        return ViewHolder(adapterView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        val action = actions[position]

        val iconView = holder.adapterView.findViewById<ImageView>(R.id.adapter_action_icon)
        iconView.setBackgroundResource(action.iconDrawable)

        val textView = holder.adapterView.findViewById<TextView>(R.id.adapter_action_title)
        textView.text = action.title

        holder.adapterView.setOnClickListener {
            action.onClick()
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = actions.size

    companion object {
        private val LOG_TAG = "SupplierActionAdapter"
    }

}

data class RecyclerAction (
        val iconDrawable: Int,
        val title: String,
        val onClick: () -> Unit
)
