package xyz.ericko.regone.ui.fragment.component.dialog.builder

import xyz.ericko.regone.R
import xyz.ericko.regone.entity.Supplier
import xyz.ericko.regone.ui.adapter.RecyclerAction
import xyz.ericko.regone.ui.fragment.component.dialog.MenuContextDialog

class SupplierMenuContextDialogBuilder (
        private val onItemUpdated: (Supplier) -> Unit,
        private val onItemDeleted: (Supplier) -> Unit
) {
    val fragmentTag = "Supplier List Menu Dialog"

    fun build(supplier: Supplier) : MenuContextDialog {
        val supplierListMenuDialog = MenuContextDialog()
        val actionList = listOf(
                RecyclerAction(R.drawable.ic_edit_black_24dp, "Edit") {
                    supplierListMenuDialog.dismiss()
                    onItemUpdated(supplier)
                },
                RecyclerAction(R.drawable.ic_delete_black_24dp, "Delete") {
                    supplierListMenuDialog.dismiss()
                    onItemDeleted(supplier)
                }
        )

        return supplierListMenuDialog.apply {
            title = supplier.name
            actions = actionList
        }
    }

}