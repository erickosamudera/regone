package xyz.ericko.regone.ui

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import xyz.ericko.regone.R
import xyz.ericko.regone.business.supplier.ActionSearchSupplier
import xyz.ericko.regone.ui.behavior.SupplierUIBehaviorBuilder
import xyz.ericko.regone.ui.fragment.page.ItemPageFragment
import xyz.ericko.regone.ui.fragment.page.PageFragment
import xyz.ericko.regone.ui.fragment.page.SearchWidgetFragment
import xyz.ericko.regone.ui.fragment.page.SupplierPageFragment

class MainActivity : AppCompatActivity() {

    private lateinit var searchFragment: SearchWidgetFragment
    private var activePage: PageFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val sprtFragmentManager = supportFragmentManager

        searchFragment = SearchWidgetFragment().apply {
            supportFragmentManager = sprtFragmentManager
            onSwitchToSearch = {
                searchFragment.apply {
                    searchAction = ActionSearchSupplier.quickCreate(context)
                    uiBehaviorBuilder = SupplierUIBehaviorBuilder()
                }
            }
        }
        fragmentManager.beginTransaction()
                .replace( R.id.fragment_search, searchFragment )
                .commit()

    }

    override fun onStart() {
        super.onStart()
        val bottomNavigation = findViewById<BottomNavigationView>(R.id.menu_navigation)
        bottomNavigation.setOnNavigationItemSelectedListener(::onMenuNavigation)
    }

    private fun onMenuNavigation( menuItem: MenuItem ) : Boolean {
        when (menuItem.itemId) {
            R.id.nav_supplier -> {
                if (activePage !is SupplierPageFragment) {
                    Log.i(LOG_TAG, "Switching to supplier page")
                    switchActivePage(SupplierPageFragment())
                }
            }
            R.id.nav_item -> {
                if (activePage !is ItemPageFragment) {
                    Log.i(LOG_TAG, "Switching to supplier page")
                    switchActivePage(ItemPageFragment())
                }
            }
            R.id.nav_price -> {}
        }
        return true
    }

    private fun switchActivePage( pageFragment: PageFragment ) {
        pageFragment.apply {
            supportFragmentManager = this@MainActivity.supportFragmentManager
        }

        activePage = pageFragment

        fragmentManager.beginTransaction()
                .replace( R.id.main_fragment_container, pageFragment )
                .commit()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        return when (item.itemId) {
//            else -> super.onOptionsItemSelected(item)
//        }
//    }

    companion object {
        private const val LOG_TAG = "MainActivity"
    }
}
