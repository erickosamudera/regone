package xyz.ericko.regone.ui.fragment.component.dialog.builder

import android.app.DialogFragment
import io.reactivex.disposables.Disposable
import xyz.ericko.regone.entity.Supplier
import xyz.ericko.regone.operation.supplier.SupplierOperations
import xyz.ericko.regone.ui.fragment.component.form.SupplierFormFragment

class SupplierEditDialogBuilder(
        private val disposableList: MutableList<Disposable>,
        private val onUpdateSuccess: () -> Unit
) {
    val fragmentTag = "Update Supplier Dialog"

    fun build(
            existingSupplier: Supplier
    ) : DialogFragment {
        return SupplierFormFragment().apply {
            this.existingSupplier = existingSupplier
            this.onConfirmListener = {
                val updateDisposable = SupplierOperations.updateSupplier(
                        context = context,
                        existingSupplier = existingSupplier,
                        supplierForm = it
                ).subscribe { changed, _ ->
                    if (changed > 0) {
                        onUpdateSuccess()
                    }
                }
                disposableList.add(updateDisposable)
            }
        }
    }
}