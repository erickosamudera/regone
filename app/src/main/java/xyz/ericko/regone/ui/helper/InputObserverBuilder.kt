package xyz.ericko.regone.ui.helper

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import xyz.ericko.regone.operation.validation.ValidationResult
import java.util.concurrent.TimeUnit

class InputObserverBuilder {
    private val inputObserver: BehaviorSubject<String> = BehaviorSubject.create()
    val observerDisposables: MutableList<Disposable> = mutableListOf()

    init {
        inputObserver
                .debounce(300, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
    }

    fun attachValidation (
            validationProcess: (String) -> Observable<ValidationResult>,
            onValidationSuccess: () -> Unit,
            onValidationFail: (String) -> Unit
    ) : InputObserverBuilder {
        val disposable = inputObserver
                .switchMap { validationProcess( it ) }
                .observeOn( AndroidSchedulers.mainThread() )
                .subscribe {
                    if (it.result) {
                        onValidationSuccess()
                    } else {
                        onValidationFail(it.error)
                    }
                }
        observerDisposables.add(disposable)
        return this
    }

    fun <T> attachAutocomplete (
            autocompleteProcess: (String) -> Observable<List<T>>,
            onAutocomplete: (List<T>) -> Unit
    ) : InputObserverBuilder {
        val disposable = inputObserver
                .switchMap { autocompleteProcess( it ) }
                .observeOn( AndroidSchedulers.mainThread() )
                .subscribe {
                    onAutocomplete( it )
                }
        observerDisposables.add(disposable)
        return this
    }

    fun build() : BehaviorSubject<String> {
        return inputObserver
    }
}