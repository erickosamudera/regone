package xyz.ericko.regone.ui.fragment.component.dialog.builder

import android.app.DialogFragment
import android.util.Log
import io.reactivex.disposables.Disposable
import xyz.ericko.regone.db.RegoneDatabase
import xyz.ericko.regone.operation.item.ItemOperations
import xyz.ericko.regone.ui.fragment.component.form.ItemFormFragment

class ItemAddDialogBuilder(
        private val disposableList: MutableList<Disposable>,
        private val onInsertSuccess: () -> Unit
) {
    companion object {
        const val LOG_TAG = "ItemAddDialogBuilder"
    }
    val fragmentTag = "Add Supplier Dialog"

    fun build() : DialogFragment {
        return ItemFormFragment().apply {
            this.onConfirmListener = {
                val insertDisposable = ItemOperations.insertItem(
                        database = RegoneDatabase.getInstance(context),
                        brandName = it.brandName,
                        shapeName = it.formName,
                        serial = it.serial
                ).subscribe { _, exception ->
                    if (exception == null) {
                        onInsertSuccess()
                    } else {
                        Log.e(LOG_TAG, "Error inserting item", exception)
                    }
                }
                disposableList.add(insertDisposable)
            }
        }
    }
}