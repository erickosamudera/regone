package xyz.ericko.regone.ui.fragment.page

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import xyz.ericko.regone.R
import xyz.ericko.regone.db.RegoneDatabase
import xyz.ericko.regone.operation.item.ItemOperations
import xyz.ericko.regone.ui.behavior.ItemUIBehavior
import xyz.ericko.regone.ui.behavior.ItemUIBehaviorBuilder
import xyz.ericko.regone.ui.fragment.component.ListFragment
import xyz.ericko.regone.ui.fragment.component.dialog.builder.ItemAddDialogBuilder

class ItemPageFragment : PageFragment() {

    private lateinit var pageLayout: View
    private lateinit var itemListFragment: ListFragment
    private lateinit var itemUIBehavior: ItemUIBehavior
    private var pageDisposables: MutableList<Disposable> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        itemListFragment = ListFragment()

        itemUIBehavior = ItemUIBehaviorBuilder().build(
                disposableList = pageDisposables,
                fragmentManager = fragmentManager,
                supportFragmentManager = supportFragmentManager,
                onInsertSuccess = {
                    print("Item inserted bro!")
                }
        )

        prepareItemList()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        pageLayout = inflater!!.inflate(R.layout.fragment_page_item, container, false)
        prepareAddItemButton()
        return pageLayout
    }

    override fun onStart() {
        super.onStart()
        displayAllItem()
    }

    private fun displayAllItem() {
        val listDisposable = ItemOperations.listAll(RegoneDatabase.getInstance(context))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { itemList ->
                    val adapter = itemUIBehavior.adapterBuilder.build(itemList)
                    itemListFragment.updateAdapterData(adapter)
                }
        pageDisposables.add(listDisposable)
    }

    private fun prepareItemList() {
        itemListFragment = ListFragment()
        childFragmentManager.beginTransaction()
                .replace( R.id.fragment_page_item_list, itemListFragment )
                .commit()
    }

    private fun prepareAddItemButton() : FloatingActionButton {
        val addButton = pageLayout.findViewById<FloatingActionButton>(R.id.fragment_page_item_button_add)
        addButton.setOnClickListener {
            val addDialogBuilder = ItemAddDialogBuilder(
                    disposableList = pageDisposables,
                    onInsertSuccess = {
                        Log.i(LOG_TAG, "Inserted!")
                    }
            )
            addDialogBuilder.build().show(childFragmentManager, addDialogBuilder.fragmentTag)
        }
        return addButton
    }

    companion object {
        private const val LOG_TAG = "ItemPageFragment"
    }
}