package xyz.ericko.regone.ui.behavior

import android.app.FragmentManager
import io.reactivex.disposables.Disposable
import xyz.ericko.regone.ui.adapter.ItemAdapterBuilder
import xyz.ericko.regone.ui.fragment.component.dialog.builder.ItemAddDialogBuilder

class ItemUIBehavior(
        val addDialogBuilder: ItemAddDialogBuilder,
        val adapterBuilder: ItemAdapterBuilder
)

class ItemUIBehaviorBuilder {

    fun build(
            disposableList: MutableList<Disposable>,
            fragmentManager: FragmentManager,
            supportFragmentManager: android.support.v4.app.FragmentManager,
            onInsertSuccess: () -> Unit = {},
            onUpdateSuccess: () -> Unit = {},
            onDeleteSuccess: () -> Unit = {}
    ) : ItemUIBehavior {
//        val editDialogBuilder = SupplierEditDialogBuilder(
//                disposableList = disposableList,
//                onUpdateSuccess = onUpdateSuccess
//        )
//        val deleteDialogBuilder = SupplierDeleteDialogBuilder(
//                disposableList = disposableList,
//                onDeleteSuccess = onDeleteSuccess
//        )
//        val listMenuDialogBuilder = SupplierMenuContextDialogBuilder(
//                onItemUpdated = {
//                    editDialogBuilder.build(it).show(fragmentManager, editDialogBuilder.fragmentTag)
//                },
//                onItemDeleted = {
//                    deleteDialogBuilder.build(it).show(fragmentManager, deleteDialogBuilder.fragmentTag)
//                }
//        )
        return ItemUIBehavior(
                addDialogBuilder = ItemAddDialogBuilder(
                        disposableList = disposableList,
                        onInsertSuccess = onInsertSuccess
                ),
                adapterBuilder = ItemAdapterBuilder {
//                        listMenuDialogBuilder.build( it ).show(supportFragmentManager, listMenuDialogBuilder.fragmentTag)
                }
        )
    }

}