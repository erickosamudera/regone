package xyz.ericko.regone.ui.fragment.component

import android.app.Fragment
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import xyz.ericko.regone.R

class ListFragment : Fragment() {

    private lateinit var fragmentView : View
    var onListDismiss : () -> Unit = {}

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        fragmentView = inflater!!.inflate(R.layout.fragment_list, container, false)

        val viewManager = LinearLayoutManager(context)
        fragmentView.findViewById<RecyclerView>(R.id.fragment_list_recycler).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
        }

        return fragmentView
    }

    fun updateAdapterData(newAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>) {
        fragmentView.findViewById<RecyclerView>(R.id.fragment_list_recycler)
                .swapAdapter(newAdapter, false)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        onListDismiss()
    }

    companion object {
        private val LOG_TAG = "ListFragment"
    }

}
