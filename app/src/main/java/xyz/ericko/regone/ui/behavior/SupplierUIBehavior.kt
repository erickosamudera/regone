package xyz.ericko.regone.ui.behavior

import android.app.FragmentManager
import io.reactivex.disposables.Disposable
import xyz.ericko.regone.ui.adapter.SupplierAdapterBuilder
import xyz.ericko.regone.ui.fragment.component.dialog.builder.SupplierAddDialogBuilder
import xyz.ericko.regone.ui.fragment.component.dialog.builder.SupplierDeleteDialogBuilder
import xyz.ericko.regone.ui.fragment.component.dialog.builder.SupplierEditDialogBuilder
import xyz.ericko.regone.ui.fragment.component.dialog.builder.SupplierMenuContextDialogBuilder

class SupplierUIBehavior(
        val addDialogBuilder: SupplierAddDialogBuilder,
        val adapterBuilder: SupplierAdapterBuilder
)

class SupplierUIBehaviorBuilder {

    fun build(
            disposableList: MutableList<Disposable>,
            onInsertSuccess: () -> Unit,
            onUpdateSuccess: () -> Unit,
            onDeleteSuccess: () -> Unit,
            fragmentManager: FragmentManager,
            supportFragmentManager: android.support.v4.app.FragmentManager
    ) : SupplierUIBehavior {
        val editDialogBuilder = SupplierEditDialogBuilder(
                disposableList = disposableList,
                onUpdateSuccess = onUpdateSuccess
        )
        val deleteDialogBuilder = SupplierDeleteDialogBuilder(
                disposableList = disposableList,
                onDeleteSuccess = onDeleteSuccess
        )
        val listMenuDialogBuilder = SupplierMenuContextDialogBuilder(
                onItemUpdated = {
                    editDialogBuilder.build(it).show(fragmentManager, editDialogBuilder.fragmentTag)
                },
                onItemDeleted = {
                    deleteDialogBuilder.build(it).show(fragmentManager, deleteDialogBuilder.fragmentTag)
                }
        )
        return SupplierUIBehavior(
                addDialogBuilder = SupplierAddDialogBuilder(
                        disposableList = disposableList,
                        onInsertSuccess = onInsertSuccess
                ),
                adapterBuilder = SupplierAdapterBuilder {
                        listMenuDialogBuilder.build( it ).show(supportFragmentManager, listMenuDialogBuilder.fragmentTag)
                }
        )
    }

}