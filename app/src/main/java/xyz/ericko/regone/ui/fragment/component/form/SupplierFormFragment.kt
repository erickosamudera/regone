package xyz.ericko.regone.ui.fragment.component.form

import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import xyz.ericko.regone.R
import xyz.ericko.regone.business.supplier.ActionCreateSupplierForm
import xyz.ericko.regone.business.supplier.ActionValidateSupplier
import xyz.ericko.regone.entity.Supplier
import xyz.ericko.regone.entity.SupplierInputForm
import xyz.ericko.regone.operation.input.Cleaner
import java.util.concurrent.TimeUnit

class SupplierFormFragment : DialogFragment() {

    lateinit var onConfirmListener: (SupplierInputForm) -> Unit
    var existingSupplier: Supplier = Supplier.generateEmpty()

    private var inputObserverDisposable : Disposable? = null

    private lateinit var addSupplierLayout: View
    private lateinit var alertDialog: AlertDialog

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialogBuilder = AlertDialog.Builder(context)

        val inflater = (context as AppCompatActivity).layoutInflater
        addSupplierLayout = inflater.inflate(R.layout.fragment_add_supplier, null)

        val nameEditText = addSupplierLayout.findViewById<EditText>(R.id.fragment_add_supplier_name)
        nameEditText.text.append(existingSupplier.name)

        alertDialog = dialogBuilder.setView(addSupplierLayout)
                .setNegativeButton( "Batal" , null )
                .setPositiveButton( getPositiveButtonText() ) { _, _ ->
                    val form = ActionCreateSupplierForm.quickCreate(nameEditText.text.toString()).doIt()
                    onConfirmListener(form)
                }.create()

        val inputObserver : BehaviorSubject<String> = BehaviorSubject.create()
        inputObserverDisposable = inputObserver.debounce(300, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
                .switchMap { ActionValidateSupplier.quickCreate(it, context).doIt().toObservable() }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    val positiveButton = alertDialog.getButton(Dialog.BUTTON_POSITIVE)
                    if (it.result) {
                        positiveButton.isEnabled = true
                    } else {
                        positiveButton.isEnabled = false
                        nameEditText.error = it.error
                    }
                }

        nameEditText.addTextChangedListener(
                SupplierTextWatcher(inputObserver)
        )

        return alertDialog
    }

    override fun onStart() {
        super.onStart()
        val positiveButton = alertDialog.getButton(Dialog.BUTTON_POSITIVE)
        positiveButton.isEnabled = false
    }

    private fun getPositiveButtonText() : String {
        return if (existingSupplier.uid == null) {
            "Tambahkan"
        } else {
            "Ubah"
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        inputObserverDisposable?.dispose()
    }

    companion object {
        private const val LOG_TAG = "SupplierFormFragment"
    }
}

class SupplierTextWatcher(
        private val inputObserver: BehaviorSubject<String>
) : TextWatcher {

    override fun afterTextChanged(s: Editable?) {
        if (s == null) {
            return
        }

        val supplierName = Cleaner.normaliseSpace(s.toString())
        inputObserver.onNext(supplierName)
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

}
