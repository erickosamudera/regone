package xyz.ericko.regone.ui.fragment.page

import android.app.Fragment
import android.support.v4.app.FragmentManager

abstract class PageFragment : Fragment() {
    lateinit var supportFragmentManager: FragmentManager
}