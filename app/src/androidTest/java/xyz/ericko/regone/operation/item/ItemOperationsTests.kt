package xyz.ericko.regone.operation.item

import android.support.test.runner.AndroidJUnit4
import junit.framework.Assert
import org.junit.Test
import org.junit.runner.RunWith
import xyz.ericko.regone.meta.DatabaseTest
import xyz.ericko.regone.meta.helper.db.DaoTestHelper


@RunWith(AndroidJUnit4::class)
class ItemOperationsTests : DatabaseTest() {

    @Test
    fun createShouldUseExistingItemFormAndBrand() {
        val brandName = "Xiaomi"
        val shapeName = "Handphone"
        val serial = "HM5Pro-6522"

        DaoTestHelper.insertNewItemBrands(mDatabase!!, listOf("Haier", brandName, "Tysonic"))
        DaoTestHelper.insertNewItemShapes(mDatabase!!, listOf("Lampu", "Steker", "Kabel", shapeName, "Termos"))

        val existingBrandId = mDatabase!!.itemBrandDao().single(brandName).blockingGet().first().uid
        val existingShapeId = mDatabase!!.itemShapeDao().single(shapeName).blockingGet().first().uid

        val newItemId = ItemOperations.insertItem(mDatabase!!, shapeName, brandName, serial).blockingGet()
        val newItem = mDatabase!!.itemDao().uid(newItemId).blockingGet().first()

        Assert.assertEquals(existingBrandId, newItem.brandId)
        Assert.assertEquals(existingShapeId, newItem.shapeId)
        Assert.assertEquals(serial, newItem.serial)
    }

    @Test
    fun createShouldCreateNewItemFormAndBrand() {
        val brandName = "Vivo"
        val shapeName = "Charger"
        val serial = "V443-6566"

        DaoTestHelper.insertNewItemBrands(mDatabase!!, listOf("Haier", "Tysonic"))
        DaoTestHelper.insertNewItemShapes(mDatabase!!, listOf("Lampu", "Steker", "Kabel", "Termos"))

        val newItemId = ItemOperations.insertItem(mDatabase!!, shapeName, brandName, serial).blockingGet()
        val newItem = mDatabase!!.itemDao().uid(newItemId).blockingGet().first()

        Assert.assertEquals(brandName, newItem.getBrand().name)
        Assert.assertEquals(3L, newItem.getBrand().uid)
        Assert.assertEquals(shapeName, newItem.getShape().name)
        Assert.assertEquals(5L, newItem.getShape().uid)
        Assert.assertEquals(serial, newItem.serial)
    }

}
