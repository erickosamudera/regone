package xyz.ericko.regone.operation.item

import android.support.test.runner.AndroidJUnit4
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import xyz.ericko.regone.meta.DatabaseTest
import xyz.ericko.regone.meta.helper.db.DaoTestHelper


@RunWith(AndroidJUnit4::class)
class ItemShapeOperationsTests : DatabaseTest() {

    @Test
    fun getExistingAndInsertNewShouldReturnNewIdIfNotExist() {
        ItemShapeOperations
                .getExistingIdOrInsertNew( mDatabase!!, "Desu" )
                .test()
                .awaitCount(1)
                .assertValue {
                    it == 1L
                }
    }

    @Test
    fun getExistingAndInsertNewShouldReturnExistingIdIfExist() {
        val formIdList = DaoTestHelper.insertNewItemShapes( mDatabase!!, listOf("Munu", "Desu") )
        ItemShapeOperations
                .getExistingIdOrInsertNew( mDatabase!!, "desu" )
                .test()
                .awaitCount(1)
                .assertValue {
                    val expectedId = formIdList[1]
                    it == expectedId
                }

    }

    @Test
    fun searchFormShouldReturnAllExistingBrands() {
        DaoTestHelper.insertNewItemShapes( mDatabase!!, listOf("Sate Ayam", "Gorengan Jagung", "Nasi Goreng Yangzhou", "Mie Kuah") )
        val searchResult = ItemShapeOperations
                .searchShape(mDatabase!!, "goreng")
                .blockingLast()
        Assert.assertEquals(2, searchResult.size)
        Assert.assertTrue(searchResult.first().name == "Gorengan Jagung")
        Assert.assertTrue(searchResult[1].name == "Nasi Goreng Yangzhou")
    }

}
