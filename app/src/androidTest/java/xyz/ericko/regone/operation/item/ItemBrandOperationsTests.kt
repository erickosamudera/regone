package xyz.ericko.regone.operation.item

import android.support.test.runner.AndroidJUnit4
import junit.framework.Assert
import org.junit.Test
import org.junit.runner.RunWith
import xyz.ericko.regone.meta.DatabaseTest
import xyz.ericko.regone.meta.helper.db.DaoTestHelper


@RunWith(AndroidJUnit4::class)
class ItemBrandOperationsTests : DatabaseTest() {

    @Test
    fun getExistingAndInsertNewShouldReturnNewIdIfNotExist() {
       ItemBrandOperations
                .getExistingIdOrInsertNew( mDatabase!!, "Desu" )
                .test()
                .awaitCount(1)
                .assertValue {
                    it == 1L
                }
    }

    @Test
    fun getExistingAndCreateNewShouldReturnExistingIfExist() {
        val brandIdList = DaoTestHelper.insertNewItemBrands( mDatabase!!, listOf("Munu", "Desu") )
        ItemBrandOperations
                .getExistingIdOrInsertNew( mDatabase!!, "desu" )
                .test()
                .awaitCount(1)
                .assertValue {
                    val expectedId = brandIdList[1]
                    it == expectedId
                }
    }

    @Test
    fun searchBrandShouldReturnAllExistingBrands() {
        DaoTestHelper.insertNewItemBrands( mDatabase!!, listOf("Sate Ayam", "Gorengan Jagung", "Nasi Goreng Yangzhou", "Mie Kuah") )
        val searchResult = ItemBrandOperations
                .searchBrand(mDatabase!!, "goreng")
                .blockingLast()
        Assert.assertEquals(2, searchResult.size)
        Assert.assertTrue(searchResult.first().name == "Gorengan Jagung")
        Assert.assertTrue(searchResult[1].name == "Nasi Goreng Yangzhou")
    }

}
