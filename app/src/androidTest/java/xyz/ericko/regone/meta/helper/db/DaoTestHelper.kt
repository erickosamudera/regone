package xyz.ericko.regone.meta.helper.db

import xyz.ericko.regone.db.RegoneDatabase
import xyz.ericko.regone.entity.ItemBrand
import xyz.ericko.regone.entity.ItemShape

object DaoTestHelper {
    fun insertNewItemBrands(
            database: RegoneDatabase,
            nameList: List<String>
    ) : List<Long> {
        return nameList.map {
            val newBrand = ItemBrand.generateNew(it)
            database.itemBrandDao().insert(newBrand)
        }
    }

    fun insertNewItemShapes(
            database: RegoneDatabase,
            nameList: List<String>
    ) : List<Long> {
        return nameList.map {
            val newForm = ItemShape.generateNew(it)
            database.itemShapeDao().insert(newForm)
        }
    }
}