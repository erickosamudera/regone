package xyz.ericko.regone.meta

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import org.junit.After
import org.junit.Before
import org.junit.Rule
import xyz.ericko.regone.db.RegoneDatabase

abstract class DatabaseTest {
    protected var mDatabase: RegoneDatabase? = null

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun initDb() {
        mDatabase = Room.inMemoryDatabaseBuilder(
                InstrumentationRegistry.getContext(),
                RegoneDatabase::class.java
        )
                .allowMainThreadQueries() // allowing main thread queries, just for testing
                .build()
        mDatabase!!.clearAllTables()
    }

    @After
    fun closeDb() {
        mDatabase!!.close()
    }
}