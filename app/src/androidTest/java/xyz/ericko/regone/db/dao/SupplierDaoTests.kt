package xyz.ericko.regone.db.dao

import android.support.test.runner.AndroidJUnit4
import junit.framework.Assert
import org.junit.Test
import org.junit.runner.RunWith
import xyz.ericko.regone.entity.Supplier
import xyz.ericko.regone.meta.DatabaseTest


@RunWith(AndroidJUnit4::class)
class SupplierDaoTests : DatabaseTest() {

    @Test
    fun testInsertSupplier() {
        val dummySupplier = Supplier.generateNew("Sinar Mas Jaya")
        val insertResult = mDatabase!!.supplierDao().insert(dummySupplier)
        Assert.assertEquals(1, insertResult)

        val anotherDummySupplier = Supplier.generateNew("Cahaya Perak Dunia")
        val anotherInsertResult = mDatabase!!.supplierDao().insert(anotherDummySupplier)
        Assert.assertEquals(2, anotherInsertResult)
    }

    @Test
    fun testSupplierSingleGet() {
        val dummySupplier = Supplier.generateNew("Sinar Mas Jaya")
        mDatabase!!.supplierDao().insert(dummySupplier)

        mDatabase!!.supplierDao()
                .single("Sinar Mas Jaya")
                .test()
                .assertValue { supplierList ->
                    supplierList.first().name == "Sinar Mas Jaya"
                }

        mDatabase!!.supplierDao()
                .single("Sinar Pak Jaya")
                .test()
                .assertValue {
                    it.isEmpty()
                }
    }

    @Test
    fun testNameStartsWith() {
        insertDummySuppliers(listOf("Sinar Mas Jaya", "Cahaya Perak", "Sinar Bulan Benderang", "Cahaya Emas"))

        mDatabase!!.supplierDao()
                .nameStartsWith("sinar")
                .test()
                .assertValue {
                    val supplier = it.first()
                    val secondSupplier = it[1]
                    it.size == 2 &&
                            supplier.name == "Sinar Bulan Benderang" &&
                            secondSupplier.name == "Sinar Mas Jaya"
                }
    }

    @Test
    fun testNameContains() {
        insertDummySuppliers(listOf("Sinar Perunggu Ayah", "Sinar Mas Jaya", "Sinar Kepala Bapak"))

        mDatabase!!.supplierDao()
                .nameContains("aya")
                .test()
                .assertValue {
                    val supplier = it.first()
                    val secondSupplier = it[1]
                    it.size == 2 &&
                            supplier.name == "Sinar Mas Jaya" &&
                            secondSupplier.name == "Sinar Perunggu Ayah"
                }

    }

    private fun insertDummySuppliers(names: List<String>) {
        names.forEach {
            val dummySupplier = Supplier.generateNew(it)
            mDatabase!!.supplierDao().insert(dummySupplier)
        }
    }

}