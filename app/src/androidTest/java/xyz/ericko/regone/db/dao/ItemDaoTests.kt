package xyz.ericko.regone.db.dao

import android.support.test.runner.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith
import xyz.ericko.regone.entity.Item
import xyz.ericko.regone.entity.ItemBrand
import xyz.ericko.regone.entity.ItemShape
import xyz.ericko.regone.meta.DatabaseTest


@RunWith(AndroidJUnit4::class)
class ItemDaoTests : DatabaseTest() {

    data class ItemSpec(
            val formName: String,
            val brandName: String,
            val serial: String
    )

    private fun generateNewItems(
            itemSpecList: List<ItemSpec>
    ) {
        itemSpecList.forEach {
            val newForm = ItemShape.generateNew(it.formName)
            val formId = mDatabase!!.itemShapeDao().insert(newForm)
            val newBrand = ItemBrand.generateNew(it.brandName)
            val brandId = mDatabase!!.itemBrandDao().insert(newBrand)
            val newItem = Item.generateNew(formId, brandId, it.serial)
            mDatabase!!.itemDao().insert(newItem)
        }
    }

    @Test
    fun testItemGetAll() {
        val itemSpecList = listOf(
                ItemSpec("Lampu", "Haier", "HA-12345"),
                ItemSpec("Steker", "Panasonic", "P4501"),
                ItemSpec("T 4 Colokan", "Midea", "MT662-2334")
        )
        generateNewItems(itemSpecList)

        mDatabase!!.itemDao()
                .all()
                .test()
                .assertValue { itemList ->
                    val checkResult = itemList.reversed().mapIndexed { index, itemQuickRead ->
                        val expectedSpec = itemSpecList[index]
                        val checkForm : Boolean = itemQuickRead.getShape().name == expectedSpec.formName
                        val checkBrand : Boolean = itemQuickRead.getBrand().name == expectedSpec.brandName
                        val checkSerial : Boolean = itemQuickRead.serial == expectedSpec.serial
                        checkForm.and(checkBrand).and(checkSerial)
                    }

                    checkResult.reduce { accumulated, current ->
                        accumulated && current
                    }
                }
    }

}