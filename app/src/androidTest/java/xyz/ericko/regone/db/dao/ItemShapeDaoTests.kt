package xyz.ericko.regone.db.dao

import android.support.test.runner.AndroidJUnit4
import junit.framework.Assert
import org.junit.Test
import org.junit.runner.RunWith
import xyz.ericko.regone.meta.DatabaseTest
import xyz.ericko.regone.meta.helper.db.DaoTestHelper

@RunWith(AndroidJUnit4::class)
class ItemShapeDaoTests : DatabaseTest() {

    @Test
    fun testExistingSingle() {
        val itemForms = listOf("Meja", "Kursi", "Kulkas", "Becak", "Bebek", "Beras")
        DaoTestHelper.insertNewItemShapes(mDatabase!!, itemForms)

        mDatabase!!.itemShapeDao()
                .single("bebek")
                .test()
                .assertValue { it.first().name == "Bebek" }
    }

    @Test
    fun testNonExistingSingle() {
        val itemForms = listOf("Meja", "Kursi", "Kulkas", "Becak", "Bebek", "Beras")
        DaoTestHelper.insertNewItemShapes(mDatabase!!, itemForms)

        mDatabase!!.itemShapeDao()
                .single("Desu")
                .test()
                .assertValue { it.isEmpty() }
    }

    @Test
    fun testNameStartsWith() {
        val forms = listOf("meja", "Steker 5 lubang", "steker 3 lubang", "lampu meja")
        DaoTestHelper.insertNewItemShapes(mDatabase!!, forms)

        mDatabase!!.itemShapeDao()
                .nameStartsWith("stek")
                .test()
                .assertValue {
                    val form = it.first()
                    val secondForm = it[1]
                    it.size == 2 &&
                            form.name == "Steker 5 lubang" &&
                            secondForm.name == "steker 3 lubang"
                }
    }

    @Test
    fun testNameContains() {
        val forms = listOf("meja terbang", "Steker 5 lubang", "steker 3 lubang", "lampu meja")
        DaoTestHelper.insertNewItemShapes(mDatabase!!, forms)

        val filteredForms = mDatabase!!.itemShapeDao()
                .nameContains("meja")
                .blockingGet()

        Assert.assertEquals(2, filteredForms.size)
        val form = filteredForms.first()
        val secondForm = filteredForms[1]
        Assert.assertEquals("lampu meja", form.name)
        Assert.assertEquals("meja terbang", secondForm.name)
    }
}