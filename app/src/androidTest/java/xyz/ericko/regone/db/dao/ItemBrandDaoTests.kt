package xyz.ericko.regone.db.dao

import android.support.test.runner.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith
import xyz.ericko.regone.meta.DatabaseTest
import xyz.ericko.regone.meta.helper.db.DaoTestHelper


@RunWith(AndroidJUnit4::class)
class ItemBrandDaoTests : DatabaseTest() {

    @Test
    fun testExistingSingle() {
        val brands = listOf("Haier", "Oppo", "Xiaomi", "Midea")
        DaoTestHelper.insertNewItemBrands(mDatabase!!, brands)

        mDatabase!!.itemBrandDao()
                .single("oppo")
                .test()
                .assertValue { it.first().name == "Oppo" }
    }

    @Test
    fun testNonExistingSingle() {
        val brands = listOf("Haier", "Oppo", "Xiaomi", "Midea")
        DaoTestHelper.insertNewItemBrands(mDatabase!!, brands)

        mDatabase!!.itemBrandDao()
                .single("Desu")
                .test()
                .assertValue { it.isEmpty() }
    }

    @Test
    fun testNameStartsWith() {
        val brands = listOf("Xiaomi", "Panasonic", "Pannacotta", "Rolex")
        DaoTestHelper.insertNewItemBrands(mDatabase!!, brands)

        mDatabase!!.itemBrandDao()
                .nameStartsWith("pan")
                .test()
                .assertValue {
                    val brand = it.first()
                    val secondBrand = it[1]
                    it.size == 2 &&
                            brand.name == "Panasonic" &&
                            secondBrand.name == "Pannacotta"
                }
    }

    @Test
    fun testNameContains() {
        val brands = listOf("Xiaomi", "Panasonic", "Tysonic", "Andalon")
        DaoTestHelper.insertNewItemBrands(mDatabase!!, brands)

        mDatabase!!.itemBrandDao()
                .nameContains("Soni")
                .test()
                .assertValue {
                    val brand = it.first()
                    val secondBrand = it[1]
                    it.size == 2 &&
                            brand.name == "Panasonic" &&
                            secondBrand.name == "Tysonic"
                }

    }
}